package TP2;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;

public class HashableString implements FamilyHashable {
	public String string;

	public HashableString(String string) {
		this.string = string;
	}

	public String toString() {
		return string;
	}

	public boolean equals(Object o) {
		if (!(o instanceof HashableString))
			return false;
		return this.string.equals(((HashableString) o).string);
	}

	@Override
	public long hashCode(int seed) {
		int bufferLength = string.length() * Character.BYTES;
		ByteBuffer buffer = MappedByteBuffer.allocate(bufferLength);
		for (int i = 0; i < string.length(); i++)
			buffer.putChar(string.charAt(i));
		buffer.flip();
		return hash(buffer, seed);
	}

	private static long horner(ByteBuffer buffer, long seed) {
		long res = 0;
		int size = buffer.remaining();
		for (int i = 0; i < size / Long.BYTES; i++)
			res = (res * seed) + buffer.getLong();
		long leftover = 0;
		while (buffer.hasRemaining()) 
			leftover = (leftover << 8) + buffer.get();
		return seed * res + leftover;
	}

	public static long hash(ByteBuffer buffer, int seed) {
		long h = horner(buffer, seed); // then compute in base 2^32
		BigInteger prod = BigInteger.valueOf(h).multiply(BigInteger.valueOf(seed));
		int length = prod.bitLength();
		if (length > 64)
			return prod.shiftRight(length - 64).longValue();
		else
			return prod.longValue();
	}

}
