package TP2;

public class TestTableCoucou {
	public static void main(String[] args) {
		CuckooTable<HashableString, Integer> courses = new CuckooTable<HashableString, Integer>(5);

		System.out.println("La table est vide ? " + courses.isEmpty()+"\n");
		
		HashableString bananes = new HashableString("bananes");
		HashableString laits = new HashableString("lait");
		HashableString fromage = new HashableString("fromage");
		HashableString yahourts = new HashableString("yahourts");

		System.out.println("Index par fonction de hachage:");
		
		courses.put(bananes, 6);
		courses.put(laits, 6);
		courses.put(fromage, 1);
		courses.put(yahourts, 24);
		
		System.out.println("\n");
		
		System.out.println("La table est vide ? " + courses.isEmpty() + "\n");
		System.out.println("yahourts est dans la table ? " + courses.containsKey(yahourts)+ "\n");

		System.out.println("fromage: " + courses.get(fromage));
		System.out.println("yahourts: " + courses.get(yahourts));
		System.out.println("bananes: " + courses.get(bananes));
		System.out.println("laits: " + courses.get(laits)+ "\n");
		
		courses.remove(yahourts);
		System.out.println("yahourts est dans la table ? " + courses.containsKey(yahourts));
	}
}
