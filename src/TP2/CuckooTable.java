package TP2;

import java.util.Vector;

public class CuckooTable<Key extends FamilyHashable, Value> {
	private class Cell {
		Key key;
		Value value;

		private Cell(Key key, Value value) {
			this.key = key;
			this.value = value;
		}

		public String toString() {
			return key.toString();
		}
	}

	private Vector<Cell> leftTable, rightTable;
	private int cardinal, tableLength;
	private int leftSeed, rightSeed;

	public CuckooTable(int expectedKeys) {
		tableLength = (int) Math.max(expectedKeys, 4);
		cardinal = 0;
		leftTable = new Vector<>(tableLength);
		rightTable = new Vector<>(tableLength);
		leftTable.setSize(tableLength);
		rightTable.setSize(tableLength);
		leftSeed = 4;
		rightSeed = 2;
	}

	public boolean isEmpty() {
		if (cardinal == 0)
		return true;
		else return false;
	}

	public int size() {
		return cardinal;
	}

	public void put(Key key, Value value) {
		Cell cell = new Cell(key, value);
		int i = leftHash(cell.key);
		int j = rightHash(cell.key);
		System.out.println(cell.key + ": " + "h1: " + i + " | h2: " + j);
		insertleftTable(cell, 0);
		cardinal++;
	}

	private void insertleftTable(Cell cell, int count) {

		if (count > (5*Math.log(tableLength)))
			return;
		else {

			int i = leftHash(cell.key);

			if (leftTable.get(i) == null) {
				leftTable.set(i, cell);
			} else {
				Cell oustcell = leftTable.get(i);
				leftTable.set(i, cell);
				insertrightTable(oustcell, count + 1);
			}
		}

	}

	private void insertrightTable(Cell cell, int count) {
		int j = rightHash(cell.key);

		if (rightTable.get(j) == null) {
			rightTable.set(j, cell);
		} else {
			Cell oustcell = rightTable.get(j);
			rightTable.set(j, cell);
			insertrightTable(oustcell, count + 1);
		}

	}

	public boolean containsKey(Key key) {

		int i = leftHash(key);
		if ((leftTable.get(i) != null) && (leftTable.get(i).key == key)) {
			return true;
		}

		int j = rightHash(key);
		if ((rightTable.get(j) != null) && (rightTable.get(j).key == key)) {
			return true;
		}

		return false;
	}

	public Value get(Key key) {	
		
		int i = leftHash(key);
		if ((leftTable.get(i) != null) && (leftTable.get(i).key == key)) {
			return leftTable.get(i).value;
		}

		int j = rightHash(key);
		if ((rightTable.get(j) != null) && (rightTable.get(j).key == key)) {
			return rightTable.get(j).value;
		}

		return null;
	}

	public void remove(Key key) {
		int i = leftHash(key);
		if ((leftTable.get(i) != null) && (leftTable.get(i).key == key)) {
			leftTable.set(i, null);
			return;
		}

		int j = rightHash(key);
		if ((rightTable.get(j) != null) && (rightTable.get(j).key == key)) {
			rightTable.set(i, null);
			return;
		}

		return;
	}

	private int leftHash(Key key) {
		return (int) Math.floorMod(key.hashCode(leftSeed), tableLength);
	}

	private int rightHash(Key key) {
		return (int) Math.floorMod(key.hashCode(rightSeed), tableLength);
	}
}
